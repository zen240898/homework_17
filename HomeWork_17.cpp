#include <iostream>
#include <math.h>

class Vector
{
private:
    double x=1;
    double y=2;
    double z=3;
public:
    void ShowCoordinates()
    {
        std::cout << x << ' ' << y << ' ' << z << "\n";
    }
    void ShowVectorLength()
    {
        double modul = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        std::cout << modul << "\n";
    }

};
int main()
{
    Vector Test;
    Test.ShowCoordinates();
    Test.ShowVectorLength();
}

